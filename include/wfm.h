#ifndef FREEDA_H 
#define FREEDA_H

#include "../libs/libfm.h"
#include "dir.h"

class Wfm {
public:

    Dir* currentDir = nullptr;//new Dir(libfm::getHome());

    Wfm(string stdPath);

    void setCurrentDir(Dir* dir);
    void start();
};

#endif // FREEDA_H 


