#ifndef UTILS_H
#define UTILS_H

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <iostream>
#include <stdlib.h>
#include <cstring>
#include <cstdio>
#include <memory>
#include <pwd.h>

#include <vector>
#include <string>

using namespace std;

bool replace(string &str, const string &from, const string &to);
string lower(string &str);
string cropLine(string &line, int limit);
bool isStringHasSpace(string &str);
void splitCommand(string &command, vector<string> &out);
void split(string &str, char delim, vector<string> &out);
pid_t CreateProcess(const char* command, const char* parametersIn);

#endif // UTILS_H
