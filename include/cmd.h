#ifndef CMD_H
#define CMD_H

#include <string>
#include <vector>
#include <map>

#include "../libs/libfm.h"
#include "utils.h"

using namespace std;

class CMD {

public:

    typedef void (*Command)(string path, vector<string> args);
    inline static string copiedPath;
    inline static string movedPath;
    inline static vector<string> result;

    CMD() {};

    bool launchCommand(string cmd, string path) {
        map<string, Command> commands = this->getCommands();
        vector<string> args; split(cmd, ' ', args);
        cmd = args[0]; args.erase(args.begin());

        if (commands.find(cmd) != commands.end()) {
            commands[cmd](path, args);   
            return true;
        }
        return false;
    }

    map<string, Command> getCommands() {
        return { 
            {"cp", this->cp}, 
            {"mv", this->mv},
            {"rm", this->rm},
            {"pt", this->pt},
            {"sh", this->sh}
        };
    }

    static void setResult(string cmd, string msg) {
        CMD::result = {cmd, msg};
    }

    static void cp(string path, vector<string> args) {
        if (args.size() > 1) {
            libfm::copy(args[0], args[1]);
            CMD::setResult("cp", "Copied from " + args[0] + " to " + args[1]);
        } else {
            CMD::setResult("cp", "Copied path " + path);
            CMD::copiedPath = path; 
        }
    }
    
    static void pt(string path, vector<string> args) {
        CMD::ptCopiedPath(path, args);
        CMD::ptMovedPath(path, args);
    }

    static void mv(string path, vector<string> args) {
        if (args.size() > 1) { 
            libfm::move(args[0], args[1]);
            CMD::setResult("mv", "Moved from " + args[0] + " to " + args[1]);
        } else {
            CMD::setResult("mv", "Copied path " + path);
            CMD::movedPath = path;
        }
    };
    
    static void rm(string path, vector<string> args) {
        if (args.size() > 0) {
            libfm::rm(args[0]);
            CMD::setResult("rm", "Remove " + args[0]);
        } else {
            CMD::setResult("rm", "Remove " + path);
            libfm::rm(path);
        }
    }

    static void sh(string path, vector<string> args) {
        if (args.size() > 0) {
            string command = "";
            for (const auto &arg : args) {
                command += arg + " ";
            }
            replace(command, "./", libfm::getDirname(path) + "/");
            system(command.c_str());
            CMD::setResult("sh", "Launch " + command);
        } else 
            CMD::setResult("sh", "Command not specified!");
    }
   

private:

    static void ptCopiedPath(string path, vector<string> args) {   
        if (CMD::copiedPath != "") {
            if (args.size() > 0) 
                libfm::copy(
                    copiedPath, 
                    libfm::getDirname(path) + "/" + args[0]
                );
            else
                libfm::copy(
                    copiedPath, 
                    libfm::getDirname(path)
                );
        }
    }

    static void ptMovedPath(string path, vector<string> args) {
        if (CMD::movedPath != "") {
            if (args.size() > 0) 
                libfm::move(
                    movedPath, 
                    libfm::getDirname(path) + "/" + args[0]
                );
            else
                libfm::move(
                    movedPath, 
                    libfm::getDirname(path) + "/" + libfm::getFilename(movedPath)
                );
        }
    }
};


#endif // CMD_H


