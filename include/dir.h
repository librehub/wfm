#ifndef DIR_H 
#define DIR_H

#include "../libs/libfm.h"

#include "entity.h"
#include "file.h"

class Dir : public Entity {
public:
  
    Dir(std::string path) : Entity(path) {}

    void open();
    std::vector<Entity*> getItems();
    std::string getInsides();

};

#endif // DIR_H 


