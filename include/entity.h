#ifndef ENTITY_H 
#define ENTITY_H

#include "../libs/libfm.h"

#include <vector>
#include <string>

//using namespace std;

class Entity {
public:
   	
    Entity(std::string path) {
        this->path = path;
    }

    std::string path;

    virtual void open() {}
    virtual std::string getName() {return libfm::getFilename(this->path);}
    virtual std::vector<Entity*> getItems() {return {};}
    virtual std::string getInsides() {return "";}

};

#endif // ENTITY_H




