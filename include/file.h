#ifndef FILE_H 
#define FILE_H

#include "../libs/libfm.h"

#include "entity.h"

class File : public Entity {
public:

    File(std::string path) : Entity(path) {}

    void open();
    std::vector<Entity*> getItems();
    std::string getInsides();

};

#endif // FILE_H 




