#include "../include/ui/mainwindow.h"
#include "../include/utils.h"
#include "../include/wfm.h"

Wfm::Wfm(string stdPath) {
    if (!stdPath.empty())
        this->setCurrentDir(new Dir(stdPath));
    else
        this->setCurrentDir(new Dir(libfm::getHome()));
}

void Wfm::setCurrentDir(Dir *dir) {
    this->currentDir = dir;
    libfm::setCurrentPath(dir->path);
}

void Wfm::start() {
    while (true) {
        Entity* result = MainWindow::pickItem(this->currentDir);
        
        if (result) {
            Dir* dir = dynamic_cast<Dir*>(result);
            if (dir/* && dir->getItems().size() > 0*/)
                this->setCurrentDir(dir);
        } else {
            break;
        }
    }
    endwin();
}

int main(int argc, char *argv[]) {
    string stdPath = "";
    if (argc > 1) {
        stdPath = argv[1];
    }

    Wfm wfm = Wfm(stdPath);
    wfm.start();

    return 0;
}

