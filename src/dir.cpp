#include "../include/dir.h"

void Dir::open() {

}

std::vector<Entity*> Dir::getItems() {
    std::vector<Entity*> items;

    for (auto dir : libfm::getDirectories(this->path)) {
        items.push_back(new Dir(dir));
    }

    for (auto file : libfm::getFiles(this->path)) {
        items.push_back(new File(file));
    }

    return items;
}

std::string Dir::getInsides() {
    return libfm::readFile(this->path);
}


