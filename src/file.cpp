#include "../include/file.h"
#include "../include/utils.h"

void File::open() {
    string command = "xdg-open";
    CreateProcess(command.c_str(), this->path.c_str());  
}

std::vector<Entity*> File::getItems() {
    return {};
}

std::string File::getInsides() {
    return libfm::readFile(this->path);
}


