
# Warlock File Manager - CLI file manager for GNU/Linux.

### Usage:
```sh
$ git clone https://codeberg.org/librehub/wfm
$ cd wfm
$ cd bin
$ sh build.sh
$ ./wfm
```

### Keybindings:
| Keybinding | Description                  |
| :---       | :---                         |
| `h`        | Open previous dir.           |
| `l`        | Open picked dir.             |
| `j`        | Move down.                   |
| `k`        | Move up.                     |
| `/`        | Search.                      |
| `:`        | Command line.                | 
| `q`        | Quite.                       |
| `d`        | Delete current file.         |
| `s`        | Open cmd with `sh` command.  | 
| `Tab`      | Autocomplete in cmd.         | 

### Commands:
| Commands  | Args                      | Example                   | Description                                                   |
| :---      | :---                      | :---                      | :---                                                          |
| `cp`      | `none` or `from`, `to`    | `cp main.cpp test.cpp`    | Copy *{from}* *{to}*. If empty - copy current file to `pt`.   |
| `mv`      | `none` or `from`, `to`    | `mv main.cpp test.cpp`    | Move *{from}* *{to}*.                                         |
| `rm`      | `path`                    | `rm main.cpp`             | Remove *{path}*.                                              |
| `pt`      |                           | `pt`                      | Paste copied file from `cp` / `mv`.                           |
| `sh`      | `command`                 | `sh firefox`              | Execute shell command (without stdout).                       |

## Contacts

| Contact                                               | Description       |
| :---:                                                 | :---              |
| [`Matrix`](https://matrix.to/#/#librehub:matrix.org)  | Matrix server.    |

## Donates
**Monero:** `47KkgEb3agJJjSpeW1LpVi1M8fsCfREhnBCb1yib5KQgCxwb6j47XBQAamueByrLUceRinJqveZ82UCbrGqrsY9oNuZ97xN`


